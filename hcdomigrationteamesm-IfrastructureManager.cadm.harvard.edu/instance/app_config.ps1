#START APP CONFIG SETUP

Try
{
    If (-Not (Test-RegistryValue -Path 'HKLM:\SOFTWARE\Automation\' -Name 'APPLICATIONCONFIG' -Value 1))
    {
        Write-Log "##########################"
        Write-Log "Starting Application Config Configuration"

        Write-Log "Installing Group Policy Management"
        Install-WindowsFeature -Name GPMC

        Write-Log "Installing Telnet Client"
        Install-WindowsFeature -Name telnet-client
    
        Write-Log "Ending Application Config Configuration"
        Write-Log "##########################"
    }
}
Catch
{
    Write-Log "Exception Message: $_.Exception.Message"
    Write-Log "Exception Item Name: $_.Exception.ItemName"
}
Finally
{
    Set-RegistryValue -Path 'HKLM:\SOFTWARE\Automation\' -Name 'APPLICATIONCONFIG' -Value 1
}