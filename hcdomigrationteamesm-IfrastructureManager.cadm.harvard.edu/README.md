IfrastructureManager
================

https://confluence.huit.harvard.edu/display/CLOPS/IfrastructureManager
-----------------------------------------------------------

Windows Automation Documentation
https://confluence.huit.harvard.edu/display/CLOPS/Windows+Automation
-----------------------------------------------------------



Notes / CloudFormation CLI examples:
================

Prod
-----------------

Prod Security Groups Stack:

    aws --region us-east-1 cloudformation create-stack --template-body file://production/cloudformation/sg.yaml --cli-input-json file://production/cloudformation/sg_parameters.json

Prod IAM Stack:

    aws  --region us-east-1 cloudformation create-stack --capabilities CAPABILITY_NAMED_IAM CAPABILITY_IAM --template-body  file://cloudformation/iam.json --cli-input-json file://production/cloudformation/iam_parameters.json


Prod S3 Contents:

    aws s3 sync instance s3://esm-prod-bucket/IfrastructureManager/instance
    aws s3 sync production s3://esm-prod-bucket/IfrastructureManager

Prod EC2 Stack:

    aws  --region us-east-1 cloudformation create-stack --template-body  file://cloudformation/ec2.yaml --cli-input-json file://production/cloudformation/ec2_parameters.json
    
Prod EC2 CloudWatch Stack:

    aws  --region us-east-1 cloudformation create-stack --template-body  file://cloudformation/ec2_cw_cf.json --cli-input-json file://production/cloudformation/ec2_cw_cf_parameters.json

Tags

	aws --region us-east-1 ec2 create-tags --resources ami-dc6467a6 i-0fa3071ac918f0607 vol-0acc90155545b9cb7 --tags file://production/cloudformation/create_tags_parameters.json